require 'rubygems'
require 'shopstyle/version'
require 'shopstyle/api'
require 'shopstyle/client'
require 'shopstyle/connection'

module Shopstyle
  class << self
    def new(options = {})
      Shopstyle::Client.new(options)
    end
  end
end
