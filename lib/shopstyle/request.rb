require 'timeout'
require 'faraday_middleware'
require 'faraday'
require 'shopstyle/helpers/timeout_error_helper'
require 'shopstyle/helpers/json_error_helper'

module Shopstyle
  module Request
    def get path, options = {}
      request :get, path, enchant_options(options)
    end

    private

    def enchant_options options
      options.merge(:format => format, :pid => pid)
    end

    def request method, path, options
      response = Timeout.timeout(timeout) do
        connection.send(method) do |request|
          request.url path, options
        end
      end
    end
  rescue Timeout::Error
    Class.new { extend TimeoutErrorHelper::ClassMethods }
  rescue MultiJson::DecodeError
    Class.new { extend JsonErrorHelper::ClassMethods }
  end
end
