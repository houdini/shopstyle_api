require 'shopstyle/api'

module Shopstyle
  class Client < Api
    def initialize(options = {})
      super options
    end
  end
end
