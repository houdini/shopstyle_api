require 'shopstyle/connection'
require 'shopstyle/configuration'
require 'shopstyle/request'

module Shopstyle
  class Api
    include Connection
    include Request

    attr_accessor *Configuration::VALID_OPTIONS_KEYS

    def initialize(options = {})
      options = Shopstyle.options.merge(options)
      Configuration::VALID_OPTIONS_KEYS.each do |key|
        send("#{key}=", options[key])
      end
    end
  end
end
