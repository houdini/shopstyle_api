require 'faraday_middleware'
require 'faraday/parse_json'

module Shopstyle
  module Connection
    private

    def connection
      options = { :headers => { :user_agent => user_agent, :accept => 'application/json' }, :url => endpoint }
      Faraday.new options do |builder|
        builder.use Faraday::Request::UrlEncoded

        builder.use Faraday::Response::Mashify
        builder.use Faraday::Response::ParseJson

        builder.response :logger

        builder.adapter(adapter)
      end
    end
  end
end
