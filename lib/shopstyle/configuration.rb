require 'shopstyle/version'

module Shopstyle
  module Configuration
    VALID_OPTIONS_KEYS = [:user_agent, :timeout, :adapter, :endpoint, :pid, :format]

    DEFAULT_ADAPTER = :net_http
    DEFAULT_ENDPOINT = 'http://api.shopstyle.com/action/'
    DEFAULT_USER_AGENT = "ruby shopstyle client gem".freeze
    DEFAULT_TIMEOUT = 5
    DEFAULT_PARTNER_ID = 'define_your_partner_id'
    DEFAULT_FORMAT = 'json2'

    attr_accessor *VALID_OPTIONS_KEYS

    def configure
      yield self
    end

    def self.extended(base)
      base.reset
    end

    def options
      options = {}
      VALID_OPTIONS_KEYS.each{|k| options[k] = send(k)}
      options
    end

    def reset
      self.adapter = DEFAULT_ADAPTER
      self.endpoint = DEFAULT_ENDPOINT
      self.timeout = DEFAULT_TIMEOUT
      self.user_agent = DEFAULT_USER_AGENT
      self.pid = DEFAULT_PARTNER_ID
      self.format = DEFAULT_FORMAT
    end
  end
  extend Configuration
end
